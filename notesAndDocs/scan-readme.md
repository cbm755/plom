## Idea and files
* After the test has been sat and the students are sending the instructors emails expressing their gratitude the tests are ready for scanning.
* We assume that the tests are scanned in colour to at least 200dpi. Don't skimp. You don't want to have to hunt down a test after the fact to rescan it. Kill disc space rather than instructor time.
* We also assume that the scan comes to the manager globbed together as PDFs. At some point we should build an alternative script which handles (say) individual TIFFs or similar.
